from redshift_client._utils import (
    NonEmptySet,
)

__version__ = "8.0.1"
__all__ = [
    "NonEmptySet",
]
